def greedy_rec_algorithm(
    array_degrees: np.ndarray[Any, Any], k_degree: int, pos_init: int, extension: int
) -> None:
    result = do_graph_anonymization(array_degrees, pos_init, extension)
    if result is None:
        return
    extension = result
    C_merge = c_merge(array_degrees, array_degrees[pos_init], k_degree)
    C_new = c_new(array_degrees, k_degree)
    if C_merge > C_new:
        # dont merge
        greedy_rec_algorithm(array_degrees, k_degree, extension, extension + k_degree)
    else:
        array_degrees[extension] = array_degrees[pos_init]
        greedy_rec_algorithm(
            array_degrees, k_degree, extension + 1, extension + 1 + k_degree
        )


def do_graph_anonymization(
    array_degrees: np.ndarray[Any, Any], pos_init: int, extension: int
) -> int | None:
    length = len(array_degrees)
    if extension >= length:
        if extension - length != 0:
            for i in range(pos_init, length):
                array_degrees[i] = array_degrees[pos_init - 1]
            return None
        else:
            extension -= 1
    for i in range(pos_init, extension):
        array_degrees[i] = array_degrees[pos_init]
    return extension
